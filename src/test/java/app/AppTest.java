package app;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
  @Test
  public void singleGoodRow() throws Exception {
    var reader = new CSVDeserializer();
    var writer = new CSVSerializer(Arrays.asList("#sum", "#prod"));
    var source = new ByteArrayInputStream("#A,#B\n3,4\n".getBytes());
    var sink = new ByteArrayOutputStream();

    new Processor(reader, writer).process(source, sink);
    assertEquals(sink.toString(), "#sum,#prod\n7.0,12.0\n");
  }

  // TODO: Fails because of parallelism.
  // @Test
  // public void twoGoodRows() throws Exception {
  //   var reader = new CSVDeserializer();
  //   var writer = new CSVSerializer(Arrays.asList("#sum", "#prod"));
  //   var source = new ByteArrayInputStream("#A,#B\n3,4\n1,5\n".getBytes());
  //   var sink = new ByteArrayOutputStream();

  //   new Processor(reader, writer).process(source, sink);
  //   assertEquals(sink.toString(), "#sum,#prod\n7.0,12.0\n6.0,5.0\n");
  // }

  @Test
  public void nonDecimal() throws Exception {
    var reader = new CSVDeserializer();
    var writer = new CSVSerializer(Arrays.asList("#sum", "#prod"));
    var source = new ByteArrayInputStream("#A,#B\na,b\n".getBytes());
    var sink = new ByteArrayOutputStream();

    new Processor(reader, writer).process(source, sink);
    assertEquals(sink.toString(), "#sum,#prod\nNaN,NaN\n");
  }

  @Test
  public void oneNonDecimal() throws Exception {
    var reader = new CSVDeserializer();
    var writer = new CSVSerializer(Arrays.asList("#sum", "#prod"));
    var source = new ByteArrayInputStream("#A,#B\na,5\n".getBytes());
    var sink = new ByteArrayOutputStream();

    new Processor(reader, writer).process(source, sink);
    assertEquals(sink.toString(), "#sum,#prod\nNaN,NaN\n");
  }

  @Test
  public void threeColumns() throws Exception {
    var reader = new CSVDeserializer();
    var writer = new CSVSerializer(Arrays.asList("#sum", "#prod"));
    var source = new ByteArrayInputStream("#A,#B,#C\n2,5,7\n".getBytes());
    var sink = new ByteArrayOutputStream();

    new Processor(reader, writer).process(source, sink);
    assertEquals(sink.toString(), "#sum,#prod\n7.0,10.0\n");
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void oneColumn() throws Exception {
    var reader = new CSVDeserializer();
    var writer = new CSVSerializer(Arrays.asList("#sum", "#prod"));
    var source = new ByteArrayInputStream("#A\n2\n".getBytes());
    var sink = new ByteArrayOutputStream();

    new Processor(reader, writer).process(source, sink);
    assertEquals(sink.toString(), "#sum,#prod\n7.0,10.0\n");
  }
}
