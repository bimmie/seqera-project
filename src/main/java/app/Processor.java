package app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;

public class Processor {
  IColumnDeserialize reader;
  IColumnSerialize writer;

  public Processor(IColumnDeserialize source, IColumnSerialize sink) {
    reader = source;
    writer = sink;
  }

  public void process(InputStream from, OutputStream to) throws IOException {
    writer.writeHeader(to);
    try {
      reader.deserializeColumn(from)
          .parallel()
          .map(Processor::processImpl)
          .forEach((processed) -> {
            try {
              writer.serialize(to, processed);
            } catch (IOException kludge) {
              throw new UncheckedIOException(kludge);
            }
          });
    } catch (UncheckedIOException e) {
      throw e.getCause();
    }
  }

  private static Processed processImpl(ArrayList<Double> row) {
    var a = row.get(0);
    var b = row.get(1);

    return new Processed(sumValues(a, b), multiplyValues(a, b));
  }

  private static double sumValues(double x, double y) { return x + y; }

  private static double multiplyValues(double x, double y) { return x * y; }
}
