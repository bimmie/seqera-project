package app;

import java.io.IOException;
import java.io.OutputStream;

public interface IColumnSerialize {
  public void writeHeader(OutputStream output) throws IOException;
  public void serialize(OutputStream output, ISerializableRow row)
      throws IOException;
}
