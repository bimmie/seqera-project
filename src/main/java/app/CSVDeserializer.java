package app;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CSVDeserializer implements IColumnDeserialize {
  public Stream<ArrayList<Double>> deserializeColumn(InputStream input) {
    return new BufferedReader(new InputStreamReader(input))
        .lines()
        .skip(1) // TODO: Yep this header skip should ideally be parameterized
        .map((line) -> {
          return (ArrayList<Double>)Stream.of(line.split(","))
              .map(CSVDeserializer::parseDouble)
              .collect(Collectors.toList());
        });
  }

  static Double parseDouble(String val) {
    try {
      return Double.parseDouble(val);
    } catch (NumberFormatException err) {
      System.out.println("Cannot translate (" + val +
                         ") to floating point: " + err.getMessage());
      return Double.NaN;
    }
  }
}
