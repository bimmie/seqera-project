package app;

public interface ISerializableRow {
  public String[] serializeRow();
}
