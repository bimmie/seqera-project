package app;

public record Processed(double sum, double prod) implements ISerializableRow {
  public String[] serializeRow() {
    return new String[] {Double.toString(sum()), Double.toString(prod())};
  }
}
