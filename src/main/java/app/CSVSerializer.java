package app;

import java.io.IOException;
import java.io.OutputStream;

public class CSVSerializer implements IColumnSerialize {
  String header;
  public CSVSerializer(Iterable<? extends CharSequence> headers) {
    header = String.join(",", headers);
  }

  public void writeHeader(OutputStream output) throws IOException {
    output.write((header + "\n").getBytes());
  }

  public void serialize(OutputStream output, ISerializableRow row)
      throws IOException {
    output.write((String.join(",", row.serializeRow()) + "\n").getBytes());
  }
}
