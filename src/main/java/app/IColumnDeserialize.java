package app;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.stream.Stream;

public interface IColumnDeserialize {
  public Stream<ArrayList<Double>> deserializeColumn(InputStream input);
}
