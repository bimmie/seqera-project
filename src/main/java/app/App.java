package app;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App {

  private static String usage() {
    return "Error: Require input and output filenames.\nUsage: App input.csv output.csv";
  }

  public static void main(String[] args) {
    if (args.length != 2) {
      System.out.println(usage());
      System.exit(1);
    }

    try (FileInputStream input = new FileInputStream(args[0]);
         FileOutputStream output = new FileOutputStream(args[1])) {
      CSVDeserializer reader = new CSVDeserializer();
      CSVSerializer writer = new CSVSerializer(Arrays.asList("#sum", "#prod"));
      new Processor(reader, writer).process(input, output);
    } catch (IOException err) {
      System.out.println(err.getMessage());
    } catch (Exception err) {
      System.out.println(err.getMessage());
    }
  }
}
