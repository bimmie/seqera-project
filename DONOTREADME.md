# Seqera Project

Build:
```bash
mvn package
```

Usage:
```bash
java -cp target/project-1.0-SNAPSHOT.jar app.App input.csv output.csv
```

With the example input.csv the expected output is:
```csv
#sum,#prod
499959.99946144037,0.0
```

## Questions

1. Am I writing out the final aggregate (`foldl`) or a rolling aggregate (`scanl`)?
2. Writing to a file seems short-sighted, prefer writing to stdout and relying on the shell?
3. What are the preecision requirements? Is `BigDecimal` necessary?

## Notes

* There's a lot of abstraction for little/no value.
* Struggling with exceptions in a lambda.


## Resources

Reading a file:
https://www.baeldung.com/reading-file-in-java

Parallel streams:
https://www.baeldung.com/java-when-to-use-parallel-stream


Initial project creation
```bash
mvn archetype:generate -DgroupId=app -DartifactId=project -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```
